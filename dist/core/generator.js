"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("./http");
var utils_1 = require("../utils");
var chalk_1 = __importDefault(require("chalk"));
var inquirer_1 = __importDefault(require("inquirer"));
var path_1 = __importDefault(require("path"));
var util_1 = __importDefault(require("util"));
var download_git_repo_1 = __importDefault(require("download-git-repo"));
var Generator = /** @class */ (function () {
    function Generator(name, targetDir) {
        //目录名称
        this.name = name;
        //创建位置
        this.targetDir = targetDir;
        //对download-git-repo进行promise化改造
        this.downloadGitRepo = util_1.default.promisify(download_git_repo_1.default);
    }
    //获取用户选择的模板
    //1)从远程拉取模板数据
    //2)用户选择自己新下载的模板名称
    //3)return用户选择的名称
    Generator.prototype.getRepo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var repoList, repos, repo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, (0, utils_1.wrapLoading)(http_1.getRepoList, 'waiting fetch template')];
                    case 1:
                        repoList = _a.sent();
                        if (!repoList)
                            return [2 /*return*/];
                        repos = repoList.map(function (item) { return item.name; });
                        return [4 /*yield*/, inquirer_1.default.prompt({
                                name: 'repo',
                                type: 'list',
                                choices: repos,
                                message: 'Please choose a template to create project',
                            })];
                    case 2:
                        repo = (_a.sent()).repo;
                        //3)return用户选择的名称
                        return [2 /*return*/, repo];
                }
            });
        });
    };
    //获取用户选择的版本
    //1)基于repo结果，远程拉取对应的tag列表
    //2)用户选择自己需要下载的tag
    //3)return用户选择的tag
    Generator.prototype.getTag = function (repo) {
        return __awaiter(this, void 0, void 0, function () {
            var tags, tagsList, tag;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, (0, utils_1.wrapLoading)(http_1.getTagList, 'waiting fetch tag', repo)];
                    case 1:
                        tags = _a.sent();
                        if (!tags)
                            return [2 /*return*/];
                        tagsList = tags.map(function (item) { return item.name; });
                        return [4 /*yield*/, inquirer_1.default.prompt({
                                name: 'tag',
                                type: 'list',
                                choices: tagsList,
                                message: 'Pleace choose a tag to crete project',
                            })];
                    case 2:
                        tag = (_a.sent()).tag;
                        //3)return用户选择的tag
                        return [2 /*return*/, tag];
                }
            });
        });
    };
    //下载远程模板
    //1)拼接下载地址
    //2)调用下载方法
    Generator.prototype.download = function (repo, tag) {
        return __awaiter(this, void 0, void 0, function () {
            var requestUrl;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        requestUrl = "xyf-cli/" + repo + (tag ? '#' + tag : '');
                        //2)调用下载方法
                        return [4 /*yield*/, (0, utils_1.wrapLoading)(this.downloadGitRepo, //远程下载方法
                            'waiting download template', //加载提示信息
                            requestUrl, //参数1：下载地址
                            path_1.default.resolve(process.cwd(), this.targetDir))];
                    case 1:
                        //2)调用下载方法
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //核心创建逻辑
    //1)获取模板名称
    //2)获取tag名称
    //3)下载模板到模板目录
    //4）模板使用提示
    Generator.prototype.create = function () {
        return __awaiter(this, void 0, void 0, function () {
            var repo, tag;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getRepo()];
                    case 1:
                        repo = _a.sent();
                        return [4 /*yield*/, this.getTag(repo)];
                    case 2:
                        tag = _a.sent();
                        //3)下载模板到模板目录
                        return [4 /*yield*/, this.download(repo, tag)];
                    case 3:
                        //3)下载模板到模板目录
                        _a.sent();
                        //4）模板使用提示
                        console.log("\r\nSuccessfully created project " + chalk_1.default.cyan(this.name));
                        console.log("\r\n  cd " + chalk_1.default.cyan(this.name));
                        console.log("  npm i\r\n");
                        return [2 /*return*/];
                }
            });
        });
    };
    return Generator;
}());
exports.default = Generator;
//# sourceMappingURL=generator.js.map