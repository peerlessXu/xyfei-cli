"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var commander_1 = require("commander");
var chalk_1 = __importDefault(require("chalk"));
var program = new commander_1.Command();
program
    .command('create <app-name>')
    .description('create a new project')
    .option('-f --force', 'overwrite target directory if it exist')
    .action(function (name, options) {
    require('./core/create')(name, options);
});
program.version("v" + require('../package.json').version);
program.on('--help', function () {
    // 新增说明信息
    console.log("\r\nRun " + chalk_1.default.cyan("xyf-cli <command> --help") + " show details\r\n");
});
program.parse(process.argv);
//# sourceMappingURL=index.js.map