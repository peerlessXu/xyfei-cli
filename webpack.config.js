const path = require("path");

module.exports = {
  entry: "./src/index.ts",
  output: {
    filename: "xyf-cli.js",
    path: path.resolve(__dirname, "dist"),
  },
  mode: 'development',
  devtool: "inline-source-map",
  module: {
    rules: [
      {
        test: /\.tsx?$/i,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  target: 'node',
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
};
