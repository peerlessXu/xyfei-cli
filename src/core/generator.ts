import { getRepoList, getTagList } from './http';
import { wrapLoading } from '../utils'
import ora from 'ora';
import chalk from 'chalk';
import inquirer from 'inquirer';
import path from 'path';
import util from 'util';
import downloadGitRepo from 'download-git-repo';

class Generator {
  name: string;
  targetDir: string;
  downloadGitRepo: any;
  constructor(name: string, targetDir: string) {
    //目录名称
    this.name = name;
    //创建位置
    this.targetDir = targetDir;
    //对download-git-repo进行promise化改造
    this.downloadGitRepo = util.promisify(downloadGitRepo);
  }

  //获取用户选择的模板
  //1)从远程拉取模板数据
  //2)用户选择自己新下载的模板名称
  //3)return用户选择的名称
  async getRepo() {
    //1)从远程拉取模板数据
    const repoList = await wrapLoading(getRepoList, 'waiting fetch template');
    if (!repoList) return;
    //过滤我们需要的模板名称
    const repos = repoList.map((item: any) => item.name);
    //2)用户选择自己新下载的模板名称
    const { repo } = await inquirer.prompt({
      name: 'repo',
      type: 'list',
      choices: repos,
      message: 'Please choose a template to create project',
    });
    //3)return用户选择的名称
    return repo;
  }

  //获取用户选择的版本
  //1)基于repo结果，远程拉取对应的tag列表
  //2)用户选择自己需要下载的tag
  //3)return用户选择的tag

  async getTag(repo: string) {
    //1)基于repo结果，远程拉取对应的tag列表
    const tags = await wrapLoading(getTagList, 'waiting fetch tag', repo);
    if (!tags) return;

    //过滤需要的tag名称
    const tagsList = tags.map((item: any) => item.name);

    //2)用户选择自己需要下载的tag
    const { tag } = await inquirer.prompt({
      name: 'tag',
      type: 'list',
      choices: tagsList,
      message: 'Pleace choose a tag to crete project',
    });

    //3)return用户选择的tag
    return tag;
  }

  //下载远程模板
  //1)拼接下载地址
  //2)调用下载方法
  async download(repo: string, tag: string) {
    //1)拼接下载地址
    const requestUrl = `xyf-cli/${repo}${tag ? '#' + tag : ''}`;
    //2)调用下载方法
    await wrapLoading(
      this.downloadGitRepo, //远程下载方法
      'waiting download template', //加载提示信息
      requestUrl, //参数1：下载地址
      path.resolve(process.cwd(), this.targetDir)
    );
  }

  //核心创建逻辑
  //1)获取模板名称
  //2)获取tag名称
  //3)下载模板到模板目录
  //4）模板使用提示
  async create() {
    //1)获取模板名称
    const repo = await this.getRepo();
    //2)获取tag名称
    const tag = await this.getTag(repo);
    //3)下载模板到模板目录
    await this.download(repo, tag);
    //4）模板使用提示
    console.log(`\r\nSuccessfully created project ${chalk.cyan(this.name)}`);
    console.log(`\r\n  cd ${chalk.cyan(this.name)}`);
    console.log(`  npm i\r\n`);
  }
}

export default Generator;
