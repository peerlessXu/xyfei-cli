import axios, { AxiosResponse } from 'axios';

axios.interceptors.response.use((response: AxiosResponse) => {
  return response.data;
});

/**
 * 获取模板信息
 * @returns {Promise}
 */
async function getRepoList() {
  return axios.get('https://api.github.com/orgs/xyf-cli/repos');
}

/**
 * 获取版本信息
 * @param {String} repo 模板名称
 * @returns {Promise}
 */
async function getTagList(repo:string) {
  return axios.get(`https://api.github.com/repos/xyf-cli/${repo}/tags`);
}

export { getRepoList, getTagList };
