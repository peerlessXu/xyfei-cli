import ora from 'ora';

//添加加载动画
export async function wrapLoading(fn: any, message: string, ...args: any) {
  //使用ora初始化，传入提示信息message
  const spinner = ora(message);
  //开始加载动画
  spinner.start();
  try {
    //执行传入方法
    const result = await fn(...args);
    //状态改为成功
    spinner.succeed('done');
    return result;
  } catch (error) {
    //状态改为失败
    spinner.fail(`${error}`);
		process.exit(1);
  }
}
