import { Command } from 'commander'; 
import chalk from 'chalk';

const program = new Command();

program
  .command('create <app-name>')
  .description('create a new project')
  .option('-f --force', 'overwrite target directory if it exist')
  .action((name, options) => {
    require('./core/create')(name, options);
  });

program.version(`v${require('../package.json').version}`);

program.on('--help', () => {
  // 新增说明信息
  console.log(
    `\r\nRun ${chalk.cyan(`xyfei <command> --help`)} show details\r\n`
  );
});

program.parse(process.argv);
