## 搭建个人简单脚手架

### 安装
```
$ npm i -g xyfei-cli
# or yarn
$ yarn global add xyfei-cli
```

### 使用
```
$ xyfei create <name> [-f|--force]
```

### 参数
- `-f,--force`: overwrite if the target exists